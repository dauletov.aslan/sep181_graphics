package kz.astana.graphicsanimationapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    private Animation animation = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView fontText = findViewById(R.id.fontTextView);
        ImageView animationImageView = findViewById(R.id.animationImageView);
        animationImageView.setBackgroundResource(R.drawable.frame_animation);
        AnimationDrawable animationDrawable = (AnimationDrawable) animationImageView.getBackground();

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.alpha:
                        animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.myalpha);
                        fontText.startAnimation(animation);
                        break;
                    case R.id.rotate:
                        animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.myrotate);
                        fontText.startAnimation(animation);
                        break;
                    case R.id.scale:
                        animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.myscale);
                        fontText.startAnimation(animation);
                        break;
                    case R.id.translate:
                        animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.mytranslate);
                        fontText.startAnimation(animation);
                        break;
                    case R.id.set:
                        animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.myset);
                        fontText.startAnimation(animation);
                        break;
                    case R.id.frame:
                        animationDrawable.start();
                        break;
                }
                return true;
            }
        });

        ImageView first = findViewById(R.id.firstImageView);
        ImageView second = findViewById(R.id.secondImageView);

        try {
            InputStream inputStream = getAssets().open("android.png");
            Drawable drawable = Drawable.createFromStream(inputStream, null);
            first.setImageDrawable(drawable);
            inputStream.close();

            inputStream = getAssets().open("arch.jpg");
            drawable = Drawable.createFromStream(inputStream, null);
            second.setImageDrawable(drawable);
            inputStream.close();
        } catch (Exception exception) {
            Log.e("Hello", exception.toString());
        }

        Button graphics = findViewById(R.id.graphicsButton);
        graphics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, GraphicsActivity.class));
            }
        });

        TextView fontFromAssets = findViewById(R.id.fontFromAssetsTextView);
        fontFromAssets.setTypeface(Typeface.createFromAsset(getAssets(), "great_vibes_regular.otf"));
    }
}