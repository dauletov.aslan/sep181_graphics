package kz.astana.graphicsanimationapp;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class GraphicsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MyGraphics myGraphics = new MyGraphics(GraphicsActivity.this);
        setContentView(myGraphics);
    }
}
