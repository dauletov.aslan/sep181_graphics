package kz.astana.graphicsanimationapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class MyGraphics extends View {

    private Paint paint;

    public MyGraphics(Context context) {
        super(context);
        paint = new Paint();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.BLUE);
        canvas.drawPaint(paint);

        drawCircle(canvas);
        drawRectangle(canvas);
        drawText(canvas);
        drawTextWithRotation(canvas);

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.android);
        drawBitmap(canvas, bitmap);
    }

    private void drawBitmap(Canvas canvas, Bitmap bitmap) {
        canvas.rotate(-45);
        canvas.drawBitmap(bitmap, 100, 800, paint);
    }

    private void drawTextWithRotation(Canvas canvas) {
        paint.setColor(Color.GRAY);
        paint.setTextSize(50f);
        int x = 30;
        int y = 40;
        canvas.rotate(45, x, y);
        canvas.drawText("Text with rotation", x, y, paint);
    }

    private void drawText(Canvas canvas) {
        paint.setColor(Color.WHITE);
        paint.setTextSize(56f);
        canvas.drawText("Hello world!", 700, 700, paint);
    }

    private void drawRectangle(Canvas canvas) {
        paint.setColor(Color.GREEN);
        canvas.drawRect(100, 250, 500, 600, paint);
    }

    private void drawCircle(Canvas canvas) {
        paint.setAntiAlias(true);
        paint.setColor(Color.YELLOW);
        canvas.drawCircle(950, 250, 100, paint);
    }
}
